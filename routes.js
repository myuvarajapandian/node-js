const fs = require('fs');

const requestHandler = (req, res) => {

    const url = req.url;
    const method = req.method;

    if (url === '/') {
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>');
        res.write('<head><title>Enter Form Details</title></head>');
        res.write('<body> <h1> Enter Form Details </h1> <form action = "/message" method ="post"> <input type = "text" name = "name"> <button type="submit">Send</button> </form> </body>');
        res.write('</html>');
        return res.end();
    }

    if (url === '/message' && method === 'POST') {
        const body = [];
        req.on('data', (chunk) => {
            body.push(chunk);
        });

        return req.on('end', () => {
            const parsedBody = Buffer.concat(body).toString();
            const message = parsedBody.split('=')[1];
            fs.writeFile('hello.txt', message, (err) => {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.end('Error saving file');
                    return;
                }
                console.log('FileWrite complete!');
                res.statusCode = 302;
                res.setHeader('Location', '/');
                return res.end();
            });
        });
    }

    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title> test </title></head>');
    res.write('<body> <h1> Hello world </h1> </body>');
    res.write('</html>');
    res.end();
};

module.exports = { handler: requestHandler, someText: 'someText' };

// exports.handler = requestHandler;
// exports.someText = 'someText';
