// const fs = require('fs');

// Create a new file in the current working directory
// fs.writeFileSync('hello.txt', 'hello world');

// const name = "yuva";
// let age = 23;
// let exists = true;

// Create a new function

// function displayUser(userName, userAge, userExists) {
//     if (userExists)
//         return 'User name is ' + userName + ', age is ' + userAge;
// }
// console.log(displayUser(name, age, exists));

// Arrow function Create a new function without function keyword
// const sum = (a, b) => {
//     return a + b;
// };

// console.log(sum(1, 2));

// object

// const perosn = {
//     name: 'yuva',
//     age: 21,
//     greet() {
//         console.log('Hello ' + this.name);
//     }
// }
// use '...' for copy all values from an object in this "..." is called as spread operator

// const student = { ...perosn, exist: true }
// console.log(perosn, student)
// console.log(perosn.greet())

// arrays
// const hobbies = ['Sports', 'Running', 'chair', 'athletes']

// for (const hobby of hobbies) {
//     console.log(hobby)
// }

// below both are same arrow functions
// console.log(hobbies.map(hobby => {
//     'hobby : ' + hobby
// }))

// console.log(hobbies.map(hobby => 'hobby : ' + hobby))

// hobbies.push('code')
// console.log(hobbies)

// slice() is used to copy all values from an array

// const hobbies1 = hobbies.slice()
// console.log(hobbies1)

// use '...' (rest operator) to get multiple arguments
// const toArray = (...args) => {
//     return args
// }

// console.log(toArray(1, 2, 3, 4, 5, 6))

// destructing

const perosn = { name: 'yuva', age: 23 }

const { name, age } = perosn;

console.log(name, age);